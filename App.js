import React from 'react';
import { StyleSheet, Button } from 'react-native';
import { View, Text } from 'react-native-tailwind'
import Prayers from './components/Prayers';
import Time from './components/Time';
import NavArrows from './components/NavArrows';

export default function App() {
  return (
    <View className="flex-1 bg-grey-light h-screen">
      <View className="bg-white p-8 m-8">
        <Time />
        <Prayers />
        <NavArrows />
      </View>
    </View>
  );
}
