import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native-tailwind'
import moment from "moment";

export default function Time() {

  const today = new Date();
  const day = moment(today).format("dddd");
  const year = moment(today).format("YYYY");
  const month = moment(today).format("MMMM");
  const date = moment(today).format('D');

  return (
    <View className="flex-row items-center pb-8">
      <View className="flex-row items-center">
        <Text className="text-4xl font-bold mr-2">{date}</Text>
        <Text className="font-bold">{month}{"\n"}
          <Text className="font-normal">{year}</Text>
        </Text>
      </View>
      <Text className="flex-1 text-right">{day}</Text>
    </View>
  );
}
