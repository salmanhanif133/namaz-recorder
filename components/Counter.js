import React, { useState, useEffect } from 'react';
import { StyleSheet, Button, CheckBox } from 'react-native';
import { View, Text } from 'react-native-tailwind'

export default function Counter({ pray, updatePrayer, checked, clear }) {
  const [isCheck, setCheck] = useState(checked)
  // const markPrayers = () => {
  //   setCheck(!isCheck)
  //   { isCheck ? updatePrayer(pray.id, false) : updatePrayer(pray.id, true) }
  // }

  useEffect(() => {


  }, [])

  return (
    <View className="flex-1">
      <View className="flex-row justify-end items-center flex-1">
        <CheckBox value={isCheck}
          onValueChange={() => {
            setCheck(!isCheck)
            { isCheck ? updatePrayer(pray.id, false) : updatePrayer(pray.id, true) }
          }} />
      </View>

    </View >
  );
}
