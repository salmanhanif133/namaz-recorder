import React from 'react';
import { StyleSheet, Button } from 'react-native';
import { View, Text } from 'react-native-tailwind'

export default function NavArrows() {
  return (
    <View className="flex flex-row justify-between mt-4">
      <Button title="Previous Day" />
      <Button title="Next Day" />
    </View>
  );
}
