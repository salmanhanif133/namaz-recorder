import React, { useState, useEffect } from 'react';
import Counter from './Counter'
import { StyleSheet, Button } from 'react-native';
import { View, Text } from 'react-native-tailwind'
import { preventAutoHide } from 'expo/build/launch/SplashScreen';
import parseErrorStack from 'react-native/Libraries/Core/Devtools/parseErrorStack';

export default function Prayers() {
  let prayers = {
    prayersArray: [
      { name: "Fajr", count: 0, id: 0, checked: false },
      { name: "Zuhur", count: 0, id: 1, checked: false },
      { name: "Asr", count: 0, id: 2, checked: false },
      { name: "Maghrib", count: 0, id: 3, checked: false },
      { name: "Isha", count: 0, id: 4, checked: false },
    ]
  }
  const [allPrayers, setPrayers] = useState(prayers);
  const clear = () => {
    prayers.prayersArray = allPrayers.prayersArray.map(prayer => {
      prayer.checked = false
      return prayer;
    })
    prayers.prayersArray.map(prayer => { console.log(prayer.name, prayer.checked) })
    console.log("clear pryaer\n")
    setPrayers(prayers);
  }

  const updatePrayer = (id, bool) => {
    prayers.prayersArray = allPrayers.prayersArray.map(prayer => {
      if (prayer.id === id) {
        if (bool) {
          prayer.count += 1;
        }
        prayer.checked = bool
      }
      return prayer
    })
    prayers.prayersArray.map(prayer => { console.log(prayer.name, prayer.checked) })
    console.log("update prayer\n")
    setPrayers(prayers);
  }

  useEffect(() => {
    //
    console.log(allPrayers.prayersArray[0].checked);

  }, [allPrayers])
  return (
    <View>
      {allPrayers.prayersArray.map((pray) =>
        <View key={pray.id} className="flex-row pb-6 items-center">
          <Text className="text-grey-darker text-xl ">{`${pray.name} ${pray.count} ${pray.checked}`}</Text>
          <Counter pray={pray} updatePrayer={updatePrayer} checked={pray.checked} clear={clear} />
        </View>)}
      <View className="flex flex-row justify-between mt-4">
        <Button title="Clear" color="red" onPress={clear} />
        <Button title="Check All" color="green" />
      </View>
    </View>
  );
}